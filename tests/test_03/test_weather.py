import pytest
import requests_mock
from weather_03.weather_wrapper import (
    WeatherWrapper,
    BASE_URL,
    FORECAST_URL,
    LOCATION_URL,
)

API_KEY = "my_api_key"


@pytest.fixture
def weather_wrapper():
    return WeatherWrapper(API_KEY)


def test_get_location_key(weather_wrapper):
    city = "TestCity"
    location_key = "12345"
    with requests_mock.Mocker() as m:
        m.get(LOCATION_URL, json=[{"Key": location_key}])
        result = weather_wrapper.get_location_key(city)
        assert result == location_key


def test_get_location_key_from_cache(weather_wrapper):
    city = "CachedCity"
    cached_location_key = "cached12345"
    weather_wrapper.location_cache[city] = cached_location_key

    # Ensure the method returns the cached value without making an API call
    result = weather_wrapper.get_location_key(city)
    assert result == cached_location_key


def test_get_location_key_city_not_found(weather_wrapper):
    city = "NonExistentCity"
    with requests_mock.Mocker() as m:
        m.get(f"{LOCATION_URL}?q={city}&apikey={API_KEY}", json=[])
        with pytest.raises(ValueError, match=f"City {city} not found"):
            weather_wrapper.get_location_key(city)


def test_get_response_city_incorrect_city(weather_wrapper):
    city = "IncorrectCity"
    url = LOCATION_URL
    with requests_mock.Mocker() as m:
        m.get(f"{url}?q={city}&apikey={API_KEY}", status_code=404)
        with pytest.raises(AttributeError, match="Incorrect city"):
            weather_wrapper.get_response_city(city, url)


def test_get_temperature(weather_wrapper):
    city = "TestCity"
    location_key = "12345"
    temperature = 20.5
    with requests_mock.Mocker() as m:
        m.get(LOCATION_URL, json=[{"Key": location_key}])
        m.get(
            BASE_URL + location_key,
            json=[{"Temperature": {"Metric": {"Value": temperature}}}],
        )
        result = weather_wrapper.get_temperature(city)
        assert result == temperature


def test_get_tomorrow_temperature(weather_wrapper):
    city = "TestCity"
    location_key = "12345"
    tomorrow_temp = 25.5
    with requests_mock.Mocker() as m:
        m.get(LOCATION_URL, json=[{"Key": location_key}])
        m.get(
            FORECAST_URL + location_key,
            json={
                "DailyForecasts": [
                    {"Temperature": {"Maximum": {"Value": 20}}},
                    {"Temperature": {"Maximum": {"Value": tomorrow_temp}}},
                ]
            },
        )
        result = weather_wrapper.get_tomorrow_temperature(city)
        assert result == tomorrow_temp


def test_find_diff_two_cities(weather_wrapper):
    city1 = "City1"
    city2 = "City2"
    location_key1 = "12345"
    location_key2 = "67890"
    temp1 = 20.5
    temp2 = 25.5
    with requests_mock.Mocker() as m:
        m.get(
            LOCATION_URL,
            [
                {"json": [{"Key": location_key1}]},
                {"json": [{"Key": location_key2}]},
            ],
        )
        m.get(
            BASE_URL + location_key1,
            json=[{"Temperature": {"Metric": {"Value": temp1}}}],
        )
        m.get(
            BASE_URL + location_key2,
            json=[{"Temperature": {"Metric": {"Value": temp2}}}],
        )
        result = weather_wrapper.find_diff_two_cities(city1, city2)
        assert result == temp1 - temp2


def test_get_diff_string(weather_wrapper):
    city1 = "City1"
    city2 = "City2"
    location_key1 = "12345"
    location_key2 = "67890"
    temp1 = 20.5
    temp2 = 25.5
    with requests_mock.Mocker() as m:
        m.get(
            f"{LOCATION_URL}?q={city1}&apikey={API_KEY}",
            json=[{"Key": location_key1}],
        )
        m.get(
            f"{BASE_URL}{location_key1}?apikey={API_KEY}",
            json=[{"Temperature": {"Metric": {"Value": temp1}}}],
        )
        m.get(
            f"{LOCATION_URL}?q={city2}&apikey={API_KEY}",
            json=[{"Key": location_key2}],
        )
        m.get(
            f"{BASE_URL}{location_key2}?apikey={API_KEY}",
            json=[{"Temperature": {"Metric": {"Value": temp2}}}],
        )
        result = weather_wrapper.get_diff_string(city1, city2)
        assert (
                result
                == (
                    f"Weather in {city1} is colder than in {city2} by "
                    f"{int(temp2 - temp1)} degrees"
                )
        )


def test_get_diff_string_warmer(weather_wrapper):
    city1 = "WarmerCity"
    city2 = "ColderCity"
    location_key1 = "12345"
    location_key2 = "67890"
    temp1 = 25.5  # Warmer city temperature
    temp2 = 20.5  # Colder city temperature
    with requests_mock.Mocker() as m:
        m.get(
            f"{LOCATION_URL}?q={city1}&apikey={API_KEY}",
            json=[{"Key": location_key1}],
        )
        m.get(
            f"{BASE_URL}{location_key1}?apikey={API_KEY}",
            json=[{"Temperature": {"Metric": {"Value": temp1}}}],
        )
        m.get(
            f"{LOCATION_URL}?q={city2}&apikey={API_KEY}",
            json=[{"Key": location_key2}],
        )
        m.get(
            f"{BASE_URL}{location_key2}?apikey={API_KEY}",
            json=[{"Temperature": {"Metric": {"Value": temp2}}}],
        )
        result = weather_wrapper.get_diff_string(city1, city2)
        assert (
                result
                == (
                    f"Weather in {city1} is warmer than in {city2} by "
                    f"{int(temp1 - temp2)} degrees"
                )
        )


def test_get_tomorrow_diff(weather_wrapper):
    city = "TestCity"
    location_key = "12345"
    today_temp = 20.5
    tomorrow_temp = 25.5
    with requests_mock.Mocker() as m:
        m.get(LOCATION_URL, json=[{"Key": location_key}])
        m.get(
            BASE_URL + location_key,
            json=[{"Temperature": {"Metric": {"Value": today_temp}}}],
        )
        m.get(
            FORECAST_URL + location_key,
            json={
                "DailyForecasts": [
                    {"Temperature": {"Maximum": {"Value": today_temp}}},
                    {"Temperature": {"Maximum": {"Value": tomorrow_temp}}},
                ]
            },
        )
        result = weather_wrapper.get_tomorrow_diff(city)
        assert (
            result
            == f"The weather in {city} tomorrow will be much warmer than today"
        )


def test_get_tomorrow_diff_warmer(weather_wrapper):
    city = "TestCity"
    location_key = "12345"
    today_temp = 20.0
    tomorrow_temp = 21.0
    with requests_mock.Mocker() as m:
        m.get(
            f"{LOCATION_URL}?q={city}&apikey={API_KEY}",
            json=[{"Key": location_key}],
        )
        m.get(
            f"{BASE_URL}{location_key}?apikey={API_KEY}",
            json=[{"Temperature": {"Metric": {"Value": today_temp}}}],
        )
        m.get(
            f"{FORECAST_URL}{location_key}?apikey={API_KEY}",
            json={
                "DailyForecasts": [
                    {"Temperature": {"Maximum": {"Value": today_temp}}},
                    {"Temperature": {"Maximum": {"Value": tomorrow_temp}}},
                ]
            },
        )
        result = weather_wrapper.get_tomorrow_diff(city)
        assert (
            result
            == f"The weather in {city} tomorrow will be warmer than today"
        )


def test_get_tomorrow_diff_much_colder(weather_wrapper):
    city = "TestCity"
    location_key = "12345"
    today_temp = 20.0
    tomorrow_temp = 16.0
    with requests_mock.Mocker() as m:
        m.get(
            f"{LOCATION_URL}?q={city}&apikey={API_KEY}",
            json=[{"Key": location_key}],
        )
        m.get(
            f"{BASE_URL}{location_key}?apikey={API_KEY}",
            json=[{"Temperature": {"Metric": {"Value": today_temp}}}],
        )
        m.get(
            f"{FORECAST_URL}{location_key}?apikey={API_KEY}",
            json={
                "DailyForecasts": [
                    {"Temperature": {"Maximum": {"Value": today_temp}}},
                    {"Temperature": {"Maximum": {"Value": tomorrow_temp}}},
                ]
            },
        )
        result = weather_wrapper.get_tomorrow_diff(city)
        assert (
            result
            == f"The weather in {city} tomorrow will be much colder than today"
        )


def test_get_tomorrow_diff_colder(weather_wrapper):
    city = "TestCity"
    location_key = "12345"
    today_temp = 20.0
    tomorrow_temp = 19.0
    with requests_mock.Mocker() as m:
        m.get(
            f"{LOCATION_URL}?q={city}&apikey={API_KEY}",
            json=[{"Key": location_key}],
        )
        m.get(
            f"{BASE_URL}{location_key}?apikey={API_KEY}",
            json=[{"Temperature": {"Metric": {"Value": today_temp}}}],
        )
        m.get(
            f"{FORECAST_URL}{location_key}?apikey={API_KEY}",
            json={
                "DailyForecasts": [
                    {"Temperature": {"Maximum": {"Value": today_temp}}},
                    {"Temperature": {"Maximum": {"Value": tomorrow_temp}}},
                ]
            },
        )
        result = weather_wrapper.get_tomorrow_diff(city)
        assert (
            result
            == f"The weather in {city} tomorrow will be colder than today"
        )


def test_get_tomorrow_diff_the_same(weather_wrapper):
    city = "TestCity"
    location_key = "12345"
    today_temp = 20.0
    tomorrow_temp = 20.0
    with requests_mock.Mocker() as m:
        m.get(
            f"{LOCATION_URL}?q={city}&apikey={API_KEY}",
            json=[{"Key": location_key}],
        )
        m.get(
            f"{BASE_URL}{location_key}?apikey={API_KEY}",
            json=[{"Temperature": {"Metric": {"Value": today_temp}}}],
        )
        m.get(
            f"{FORECAST_URL}{location_key}?apikey={API_KEY}",
            json={
                "DailyForecasts": [
                    {"Temperature": {"Maximum": {"Value": today_temp}}},
                    {"Temperature": {"Maximum": {"Value": tomorrow_temp}}},
                ]
            },
        )
        result = weather_wrapper.get_tomorrow_diff(city)
        assert (
            result
            == f"The weather in {city} tomorrow will be the same than today"
        )
