import pytest
import os
import tempfile
from tree_utils_02.size_tree import SizeTree
from tree_utils_02.size_node import FileSizeNode


@pytest.fixture
def temp_directory():
    with tempfile.TemporaryDirectory() as tempdir:
        yield tempdir


def test_construct_filenode_file(temp_directory):
    size_tree = SizeTree()
    file_path = os.path.join(temp_directory, "test_file.txt")
    with open(file_path, "w") as f:
        f.write("Hello, World!")

    file_node = size_tree.construct_filenode(file_path, is_dir=False)
    assert isinstance(file_node, FileSizeNode)
    assert file_node.name == "test_file.txt"
    assert file_node.is_dir is False
    assert file_node.size == os.path.getsize(file_path)


def test_construct_filenode_directory(temp_directory):
    size_tree = SizeTree()
    dir_path = os.path.join(temp_directory, "test_dir")
    os.makedirs(dir_path)

    file_node = size_tree.construct_filenode(dir_path, is_dir=True)
    assert isinstance(file_node, FileSizeNode)
    assert file_node.name == "test_dir"
    assert file_node.is_dir is True
    assert file_node.size == 4096


def test_update_filenode(temp_directory):
    size_tree = SizeTree()
    dir_path = os.path.join(temp_directory, "test_dir")
    os.makedirs(dir_path)
    file_path = os.path.join(dir_path, "test_file.txt")
    with open(file_path, "w") as f:
        f.write("Hello, World!")

    dir_node = size_tree.construct_filenode(dir_path, is_dir=True)
    file_node = size_tree.construct_filenode(file_path, is_dir=False)
    dir_node.children.append(file_node)

    updated_node = size_tree.update_filenode(dir_node)
    assert updated_node.size == 4096 + os.path.getsize(file_path)
