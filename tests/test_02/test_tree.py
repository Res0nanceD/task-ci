import pytest
import os
import tempfile
from tree_utils_02.tree import Tree
from tree_utils_02.node import FileNode


@pytest.fixture
def temp_directory():
    with tempfile.TemporaryDirectory() as tempdir:
        yield tempdir


def test_get_file(temp_directory):
    tree = Tree()
    file_path = os.path.join(temp_directory, "test_file.txt")
    with open(file_path, "w") as f:
        f.write("Hello, World!")

    node = tree.get(file_path, dirs_only=False)
    assert isinstance(node, FileNode)
    assert node.name == "test_file.txt"
    assert node.is_dir is False
    assert node.children == []

    with pytest.raises(AttributeError):
        tree.get(file_path, dirs_only=True)


def test_get_file_dirs_only_recurse(temp_directory):
    tree = Tree()
    file_path = os.path.join(temp_directory, "test_file.txt")
    with open(file_path, "w") as f:
        f.write("Hello, World!")

    node = tree.get(file_path, dirs_only=True, recurse_call=True)
    assert node is None


def test_get_file_dirs_only_norecurse(temp_directory):
    tree = Tree()
    file_path = os.path.join(temp_directory, "test_file.txt")
    with open(file_path, "w") as f:
        f.write("Hello, World!")

    with pytest.raises(AttributeError, match="Path is not directory"):
        tree.get(file_path, dirs_only=True, recurse_call=False)


def test_get_directory(temp_directory):
    tree = Tree()
    dir_path = os.path.join(temp_directory, "test_dir")
    os.makedirs(dir_path)

    node = tree.get(dir_path, dirs_only=False)
    assert isinstance(node, FileNode)
    assert node.name == "test_dir"
    assert node.is_dir is True


def test_get_nonexistent_path():
    tree = Tree()
    with pytest.raises(AttributeError, match="Path not exist"):
        tree.get("/nonexistent/path", dirs_only=False)


def test_get_directory_with_files(temp_directory):
    tree = Tree()
    dir_path = os.path.join(temp_directory, "test_dir")
    os.makedirs(dir_path)

    subdir_path = os.path.join(dir_path, "subdir")
    os.makedirs(subdir_path)

    file_path = os.path.join(dir_path, "test_file.txt")
    with open(file_path, "w") as f:
        f.write("Hello, World!")

    node = tree.get(dir_path, dirs_only=False)
    assert isinstance(node, FileNode)
    assert node.name == "test_dir"
    assert node.is_dir is True
    assert len(node.children) == 2

    subdir_node = next(
        (child for child in node.children if child.name == "subdir"), None
    )
    file_node = next(
        (child for child in node.children if child.name == "test_file.txt"),
        None,
    )

    assert subdir_node is not None
    assert subdir_node.is_dir is True
    assert len(subdir_node.children) == 0

    assert file_node is not None
    assert file_node.is_dir is False
    assert len(file_node.children) == 0


def test_get_directory_dirs_only(temp_directory):
    tree = Tree()
    dir_path = os.path.join(temp_directory, "test_dir")
    os.makedirs(dir_path)

    subdir_path = os.path.join(dir_path, "subdir")
    os.makedirs(subdir_path)

    file_path = os.path.join(dir_path, "test_file.txt")
    with open(file_path, "w") as f:
        f.write("Hello, World!")

    node = tree.get(dir_path, dirs_only=True)
    assert isinstance(node, FileNode)
    assert node.name == "test_dir"
    assert node.is_dir is True
    assert len(node.children) == 1

    subdir_node = next(
        (child for child in node.children if child.name == "subdir"), None
    )

    assert subdir_node is not None
    assert subdir_node.is_dir is True
    assert len(subdir_node.children) == 0


def test_filter_empty_nodes(temp_directory):
    base_path = temp_directory
    tree = Tree()

    os.makedirs(os.path.join(base_path, "empty_dir"))
    os.makedirs(os.path.join(base_path, "non_empty_dir"))
    file_path = os.path.join(base_path, "non_empty_dir", "file.txt")
    with open(file_path, "w") as f:
        f.write("Hello, World!")

    root_node = tree.construct_filenode(base_path, is_dir=True)
    empty_node = tree.construct_filenode(
        os.path.join(base_path, "empty_dir"), is_dir=True
    )
    non_empty_node = tree.construct_filenode(
        os.path.join(base_path, "non_empty_dir"), is_dir=True
    )
    file_node = tree.construct_filenode(
        os.path.join(base_path, "non_empty_dir", "file.txt"), is_dir=False
    )

    non_empty_node.children.append(file_node)
    root_node.children.append(empty_node)
    root_node.children.append(non_empty_node)

    tree.filter_empty_nodes(root_node, base_path)

    assert not os.path.exists(os.path.join(base_path, "empty_dir"))
    assert os.path.exists(os.path.join(base_path, "non_empty_dir"))
    assert os.path.exists(os.path.join(base_path, "non_empty_dir", "file.txt"))


def test_filter_empty_nodes_root_error():
    tree = Tree()

    # Create a root node with no children
    root_node = tree.construct_filenode(".", is_dir=True)

    # Expecting ValueError when calling filter_empty_nodes on the root path
    with pytest.raises(ValueError, match="Code should not be executed here!"):
        tree.filter_empty_nodes(root_node, ".")
