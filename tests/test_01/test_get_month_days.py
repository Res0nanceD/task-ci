import pytest
import re
from simple_library_01.functions import get_month_days


def test_get_month_days():
    # Проверка 1930
    assert get_month_days(1930, 1) == 30
    # Проверка високосного года
    assert get_month_days(2020, 2) == 29
    # Проверка не високосного года
    assert get_month_days(2019, 2) == 28

    assert get_month_days(2020, 2) == 29
    assert get_month_days(2019, 2) == 28
    assert get_month_days(2020, 4) == 30
    assert get_month_days(2020, 7) == 31
    assert get_month_days(2020, 11) == 30

    with pytest.raises(
        AttributeError, match=re.escape("Month should be in range [1-12]")
    ):
        get_month_days(2020, 0)

    with pytest.raises(
        AttributeError, match=re.escape("Month should be in range [1-12]")
    ):
        get_month_days(2020, 13)
