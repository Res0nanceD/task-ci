import pytest
from simple_library_01.functions import is_leap


def test_is_leap():
    assert is_leap(2000) is True
    assert is_leap(1900) is False
    assert is_leap(2020) is True
    assert is_leap(2019) is False

    with pytest.raises(AttributeError, match="Year must be greater than 0"):
        is_leap(0)
    with pytest.raises(AttributeError, match="Year must be greater than 0"):
        is_leap(-100)
